<!DOCTYPE html>
<html>
<head>
    <title>Data Type</title>
</head>
<body>
    <h1>Tipovi podataka</h1>
    <p><b>String</b> – niske, mogu se koristiti jednostuki ili dvostruki navodnici</p>
    <pre>$string="Hello World!"</pre>
    <?php
        $string="Hello World!";
        echo "<span style='color:red'> " .$string. "</span>";
    ?>


    <p><b>Integer</b> – podržan dekadni, oktalni i heksadekadni zapis broja </p>
    <pre>$int=123456</pre>
    <?php
        $int=123456;
        echo "<span style='color:green'> " .$int. "</span>";
    ?>


    <p><b>Float</b> – broj sa decimalnom tačkom </p>
    <pre>$float=125.78</pre>
    <?php
        $float=125.78;
        echo "<span style='color:blue'> " .$float. "</span>";
    ?>

    <p><b>Boolean</b> – logički tip:  vrednosti true i false </p>
    <pre>$bool=True</pre>
    <?php
        $bool=TRUE;
        echo "<span style='color:orange'> " .$bool. "</span>";
    ?>

    <p><b>Array</b> – nizovi, zadaju se sa array(...)</p>
    <pre>$niz=array (1, 2, 3, 4)</pre>
    <?php
        function red ($niz) {
            foreach ($niz as $value) {
                echo "<span style='color:rgb(109, 5, 40)'> " .$value. "</span>"." ";
            }
        }
        $niz1 = array(1, 2, 3, 4);
        red($niz1);
    ?>

    <h1>Include Function</h1>
    <?php 
        include 'functions.php';
        $new_numbers1 = array (1, 27, 5, 8, 181);
        colornumb($new_numbers1, 'red');

    ?>

    <h1>Include Function 1</h1>
    <?php 
        include 'functions1.php';
        $new_numbers1 = array (1, 27, 5, 8, 181);
        colornumb1($new_numbers1, 'red');

    ?>
    
    <h1>Include Function Max</h1>
    <?php 
        include 'maxfunct.php';
        $numbe = array(10, 20, 52, 105, 56, 89, 96);
        max_numb1($numbe, 'green');
    ?>
    <h1>Include Function Min</h1>
    <?php 
        include 'minfunct.php';
        $numbe = array(10, 20, 52, 105, 56, 89, 96);
        min_numb1($numbe, 'red');
    ?>


    
</body>
</html>